package com.example.androidstudio.prelimorgaafternoon_1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private int mRandomNumber;
    private EditText mInputText;
    private Button mButton;
    private Button mAnswerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mInputText = (EditText) findViewById(R.id.input_edit_text);
        mButton = (Button) findViewById(R.id.button);
        mAnswerButton = (Button) findViewById(R.id.answer_button);

        mRandomNumber = new Random().nextInt(20) + 1;

        setButtonListener();
        setAnswerButtonListener();
    }

    private void setButtonListener() {
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String inputStr = mInputText.getText().toString();

                if (!inputStr.matches("[0-9]+")) {
                    Toast.makeText(getApplicationContext(), "Numeric input only", Toast.LENGTH_SHORT).show();
                    return;
                }

                int input = Integer.parseInt(inputStr);

                if (input < mRandomNumber) {
                    Toast.makeText(getApplicationContext(), "Higher!", Toast.LENGTH_SHORT).show();
                }
                else if (input > mRandomNumber) {
                    Toast.makeText(getApplicationContext(), "Lower...", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Congrats! The number is " + mRandomNumber, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setAnswerButtonListener() {
        mAnswerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Answer is " + mRandomNumber, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
